// Assignment 4

function charReverseHandler (letters) {
    var newLetters = "";
    for(var i = 0; i<letters.length; i++){
        if(letters[i] === letters[i].toLowerCase()){
            newLetters += letters[i].toUpperCase();
        }else {
            newLetters += letters[i].toLowerCase();
        }
    }
    console.log(newLetters);
    return newLetters;
}



function createListHandler () {
    if (document.getElementById("noteText").value == "") {
        alert("insert note please!");
    } else {
        var insertNote ;
        var container_block ;
         
        insertNote = document.createElement( 'div' );
        insertNote.className="todonote";
        insertNote.innerHTML = document.getElementById("noteText").value
         
        container_block = document.getElementById( 'list' );
        container_block.appendChild( insertNote );
        document.getElementById("noteText").value = "";
    }
    
}

function findDublicatedHandler (arry) {
    const uniqueElements = new Set(arry);
    console.log(uniqueElements);
    const filteredElements = arry.filter(item => {
        if (uniqueElements.has(item)) {
            uniqueElements.delete(item);
        } else {
            return item;
        }
    });

    return [...new Set(filteredElements)]
}

function unionHandler (arrA, arrB) {
    return [...new Set([...arrA, ...arrB])];
}

function removeHandler (arry) {
    let removed = [];
    for (let i of arry) {
        if (i!='null' && i!='0' && i!='""' && i!='false'&& i!='undefined' && i!='NaN') {
            removed.push(i);
        }
    }
    return removed;
}

function reverseStringHandler (str) {
    return str.split('').sort().join('');
}